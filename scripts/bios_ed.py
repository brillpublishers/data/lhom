from os import path
import regex
from lxml import etree

data_dir = path.join('..','data','0668IbnAbiUsaibia','Tabaqatalatibba')
fname = path.join(data_dir,'0668IbnAbiUsaibia.Tabaqatalatibba.lhom-ed-ara1.xml')

re_bios = regex.compile(r'\n(\t+)<p style="direction:rtl; unicode-bidi:embed"><hi rend="bold">(?:\[&#x202D;|&#x202D;\[)(\d+).(\d+)(?:&#x202C;\]|\]&#x202C;).+?</p>')

re_chapter = regex.compile(r'<div type="textpart" subtype="chapter" n="\d+">.+?\n\t\t\t</div>', flags = regex.DOTALL)

def format_opening(match):
    return f'{match.group()}\n\t\t\t<div type="textpart" subtype="biography" n="0">'

def format_bio(match):
    bio = match.group()
    tabs = match.group(1)
    n = match.group(3)
    return f'\n{tabs[:-1]}</div>\n{tabs[:-1]}<div type="textpart" subtype="biography" n="{n}">{bio}'

def format_chapter(match):
    chapter = match.group()
    chapter = re_bios.sub(format_bio,chapter)
    return regex.sub(r'^<div.+?>',format_opening, chapter) + "\n\t\t\t</div>"

if __name__ == "__main__":
    with open(fname) as f:
        content = f.read()

    bios = re_bios.findall(content)
    # Exceptions: 1.3, 6.1, 6.3, 6.5
    print(len(bios))

    new_content = re_chapter.sub(format_chapter, content)
    with open(fname,'w') as f:
        f.write(new_content)
